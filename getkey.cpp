//
// Created by ian on 3/14/19.
//

#include "HashTable.h"

template<class t>
unsigned long long int
HashTable<t>::getkey(unsigned long long input) {
    return( times_hashed * ((A * input * input * input) + (B * input * input) + (C * input) + D));
}

template<class t>
unsigned long long int
HashTable<t>::getkey(char *input) {
    unsigned long long int temp = 0;
    for(char* iter = input; iter; ++iter)
        temp *= static_cast<unsigned long long int>(*iter);

    return(temp);
}

template<class t>
unsigned long long int
HashTable<t>::getkey(std::string input) {
    unsigned long long int temp = 0;
    auto iter2 = input.end();
    for(auto iter = input.begin(); iter != iter2; ++iter)
        temp *= static_cast<unsigned long long int>(*iter);
    return(temp);
}
