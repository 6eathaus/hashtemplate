//
// Created by ian on 3/14/19.
//

#include "HashTable.h"
#include <math.h>
#include <iostream>

template<class t>
bool HashTable<t>::insert(char *cstr,
                          t item) {
    unsigned long long int temp = getkey(cstr);
    KEYORDER.emplace_back(temp);
    return( insertFinal( temp, item ));
}


template<class t>
bool HashTable<t>::insert(std::string i, t item) {
    unsigned long long int temp = getkey(i);
    KEYORDER.emplace_back(temp);
    return( insertFinal( temp, item ));
}


template<class t>
std::vector<t> HashTable<t>::insert(
        std::vector<unsigned long long int> Key,
        std::vector<t> item) {
    if(item.size() != Key.size())
        throw std::runtime_error("Fail Vecter Insertion: parameter vectors of differing lengths");

    std::vector<t> fails;

    auto j = Key.begin();
    for( auto i : item){
        if(!insert(*j, *i))
            fails.emplace_back(*i);
        ++j;
    }
    return fails;
}

template<class t>
std::list<t>
HashTable<t>::insert(std::list<unsigned long long int> Key,
                     std::list<t> item) {

    if(item.size() != Key.size())
        throw std::runtime_error("Fail Vecter Insertion: parameter vectors of differing lengths");

    if( (!getNoHash()) && TOTALSIZE < Key.size() + SIZE ){
    }

    std::list<t> fails;

    auto j = Key.begin();
    for( auto i : item){
        if(!insert(*j, *i))
            fails.emplace_back(*i);
        ++j;
    }
    return fails;
}