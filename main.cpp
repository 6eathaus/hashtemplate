#include <iostream>
#include <cassert>
#include "HashTable.h"

int main() {
    {
        std::cout << "Testing Constructor<int> and Deconstructor:\n";
        HashTable<int> t;
        assert(t.totalSize() == 409 );
        std::cout << "Constructor: Pass\n";
    }
    std::cout << "Deconstructor: Pass\n\n";
    {
        int i = 5;
        std::cout << "Testing insert Linear:\n";
        HashTable<int> t;
        t.insert(std::pair<unsigned long long int, int>(i,i));
        std::cout << "Testing insert Linear: Pass\n\n";
    }
    {
        int i = 5;
        std::cout << "Testing Accessor\n";
        std::pair<int, bool> test(i,true);
        HashTable<int> t;
        t.insert(std::pair<unsigned long long int, int>(5,i));
        assert(t[i] == test);
        std::cout << "Accessor at Location 5 is 5.\n";
        std::cout << "Testing Accessor: Pass\n\n";
    }
    {
        int i = 5;
        std::cout << "Testing Accessor of uninserted area.\n";
        std::pair<int, bool> test(int(),false);
        HashTable<int> t;
        assert(!(t[4].second));
        std::cout << "Accessor at Location 4 is not there.\n";
        std::cout << "Testing Accessor of uninserted area: Pass\n\n";
    }
    {
        int i = 5;
        std::cout << "Testing Accessor of Deleted area.\n";
        std::pair<int, bool> test(int(),false);
        HashTable<int> t;
        t.insert(std::pair<unsigned long long int, int>(5,i));
        t.clear(i);
        assert(!(t[5].second));
        std::cout << "Testing Accessor of Deleted area: Pass\n\n";

    }
    {
        int i = 5;
        std::cout << "Testing Accessor of unDeleted area.\n";
        std::pair<int, bool> test(int(),false);
        HashTable<int> t;
        t.insert(std::pair<unsigned long long int, int>(5,i));
        t.clear(i);
        t.insert(std::pair<unsigned long long int, int>(5,i));
        assert((t[5].second));
        std::cout << "Testing Accessor of unDeleted area: Pass\n\n";

    }
    {
        int i = 5;
        std::cout << "Testing Accessor of Collision area.\n";
        std::pair<int, bool> test(int(),false);
        HashTable<int> t;
        t.insert(std::pair<unsigned long long int, int>(i,i));
        t.insert(std::pair<unsigned long long int, int>(i,i));
        assert((t[5].second) && t[5].first == 5);
        std::cout << "Location 5 is: " << t[5].first << "\n";
        assert((t[10].second) && t[10].first == 5);
        std::cout << "Location 10 is: " << t[10].first << "\n";

        std::cout << "Testing Accessor of Collision area: Pass\n\n";
    }
    {
        std::cout << "Testing vector insert runtime check:\n";
        std::vector<unsigned long long int> Keys = {1,2,3,4,5};
        std::vector<int> Items = {1,2,3,4};
        std::vector<int> Fails;
        HashTable<int> t;
        try {
            t.insert(Keys,Items, Fails);
        }
        catch(std::runtime_error& a){
            std::cout << a.what() << "\n";
            std::cout << "Testing vector insert runtime check: Passed";
        }
        std::cout << "\n\n";
    }
    {

        std::cout << "Testing vector insert:\n";
        HashTable<int> t;
        std::vector<unsigned long long int> Keys = {1,2,3,4,5};
        std::vector<int> Items = {1,2,3,4,5};
        std::vector<int> Fails;
        t.insert(Keys, Items, Fails);
        assert(!Fails.size());
        std::cout << "Number of fails is: " << Fails.size() << "\n";
        assert(t[1].first == 1);
        std::cout << "Position 1 is: " << t[1].first << "\n";
        assert(t[2].first == 2);
        std::cout << "Position 2 is: " << t[2].first << "\n";
        assert(t[3].first == 3);
        std::cout << "Position 3 is: " << t[3].first << "\n";
        assert(t[4].first == 4);
        std::cout << "Position 4 is: " << t[4].first << "\n";
        assert(t[5].first == 5);
        std::cout << "Position 5 is: " << t[5].first << "\n";
        std::cout << "Testing vector insert: Passed\n\n";
    }
    {
        std::cout << "Testing vector insert:\n";
        std::vector<unsigned long long int> Keys = {1,2,3,4,5};
        std::vector<int> Items = {1,2,3,4,5};
        std::vector<int> Fails;
        HashTable<int> t;
        t.insert(Keys,Items, Fails);
        std::cout << "Testing vector insert: Passed\n\n";

    }
    {
        std::cout << "Testing Rehash:\n";
        HashTable<int> t;

        std::cout << "Creating vector key of 409 with key 1.\n";
        std::vector<unsigned long long int> Key(409, 1);

        std::cout << "Creating vector item of 409 with value 1.\n";
        std::vector<int> Items(409, 1);
        std::cout << "Creating a fail vector.\n";
        std::vector<int> Fails;
        assert(t.totalSize() == 409);
        t.insert(Key,Items,Fails);
        for(unsigned long long int i = 1; i < 410; ++i)
            assert(t[i].first == 1);
        assert(t.totalSize() > 409);
        std::cout << "The size of the hash is now: " << t.totalSize() << "\n";
        std::cout << "Testing Rehashing: Passed";
    }
    return 0;
}

