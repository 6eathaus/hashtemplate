//
// Created by ian on 3/12/19.
//

#include "HashTable.h"

namespace HashName{
    const char nohash[] = "nohash";
    const char dual[] = "dual";

    const char *args[args_size] = {nohash,dual};
}



template<class t>
void HashTable<t>::rehash(unsigned long long int temp) {
    /// Create containers for rehash.
    t Data[TOTALSIZE];
    bool Flags[TOTALSIZE];


    // Create iterators for container and DATA.
    std::pair<unsigned long long int, t>* data = Data;
    std::pair<unsigned long long int, t>* dat = DATA;

    /// Go through
    while(data && dat){
        *data = *dat;
        ++data;
        ++dat;
    }


    /// create iterators for container and FLAGS
    bool* point = FLAG;
    bool* Point = Flags;

    /**
     * Go through the each bool in FLAGS and set
     * the bits in each Bool of it to the same
     * As in the container array.
     *
     * We are doing this because this moves all
     * flags put into the FLAGS variables to
    */
    while(point && Point){
        *Point |= *point;
        ++Point;
        ++point;
    }

    TOTALSIZE = nextPrime(temp *4);

    delete DATA;
    DATA = new std::pair<unsigned long long int, t>[TOTALSIZE];


    delete FLAG;
    FLAG = new bool[TOTALSIZE];
}



template<class t>
bool
HashTable<t>::getDeleted(unsigned long long int) {
    return false;
}

template<class t>
bool
HashTable<t>::getActive(unsigned long long int) {
    return false;
}





