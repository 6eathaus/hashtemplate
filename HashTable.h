//
// Created by ian on 3/12/19.
//

#ifndef HASHTABLE_HASHTABLE_H
#define HASHTABLE_HASHTABLE_H
#pragma once

#include <vector>
#include <list>
#include <string>

namespace HashName {
    const int args_size = 2;

    extern const char nohash[];
    extern const char dual[];

    extern const char *args[args_size];
}



template <class t>
class HashTable {

/*************************************************Variables*********************************************************/
    /**
     * This is to create a hash of t type with two
     * checks for each, delete, and inserted.
     */
    std::pair<unsigned long long int, t>* DATA;
    unsigned char* FLAG;

    std::vector<unsigned long long int> KEYORDER;

    unsigned int times_hashed;

    unsigned char TYPE;

    unsigned long long int SIZE;
    unsigned long long int TOTALSIZE;

    unsigned long long int A, B, C, D;

/*************************************************Private Functions************************************************/

    void rehash(unsigned long long int);

    unsigned long long int getKey(unsigned long long int input);

//    unsigned long long int getkey(char *input);
//
//    unsigned long long int getkey(std::string input);

    bool insertFinal(unsigned long long int Key, std::pair<unsigned long long int,t>& item);

    bool getNoHash(){return static_cast<bool>(TYPE & 0x2);};

    bool INSERTFLAG(unsigned long long int temp){return static_cast<bool>(FLAG[temp%TOTALSIZE] & 0x1);};

    void SETINSERTFLAG(unsigned long long int temp){FLAG[temp%TOTALSIZE] |= 1;};

    bool DELETEFLAG(unsigned long long int temp){return static_cast<bool>(FLAG[temp%TOTALSIZE] >> 1);};

    void SETDELETEFLAG(unsigned long long int temp){FLAG[temp%TOTALSIZE] |= 1 << 1;};

    void SETUNDELETEFLAG(unsigned long long int temp){FLAG[temp%TOTALSIZE] &= 13; };

    void getAllofKey(unsigned long long int k, std::vector<t>& ret);

    bool insertNoHash( std::pair<unsigned long long int, t> item);

    bool insertNoHash( unsigned long long int, t item);


    bool isPrime(unsigned long long int temp);

    unsigned long long int nextPrime(unsigned long long int b);

    bool getDual();

public:

    HashTable(): FLAG(new unsigned char[409]{0}),
                 DATA(new std::pair<unsigned long long int, t>[409]),
                 TOTALSIZE(409), SIZE(0), A(0), B(0), C(0), D(0),
                 times_hashed(1), TYPE(0){};

//    HashTable(char** args, int argn, long long int d = 0, long long int c = 0, long long int b = 0, unsigned long long int a=0);
//
    virtual ~HashTable();
//
    unsigned long long int totalSize(){return TOTALSIZE;};
//
    unsigned long long int size(){return SIZE;};
//
//    bool getDeleted(unsigned long long int);
//
//    bool getActive(unsigned long long int);
//
    std::pair<t,bool> operator[](unsigned long long int Key);
//
//    std::pair<t,bool> operator[](std::string Key);
//
//    std::pair<t,bool> operator[](char *Key);
//
//    bool insert(char* cstr, t item);
//
    bool insert( unsigned long long int, t item);

    bool insert( std::pair<unsigned long long int, t> item);

    void clear(unsigned long long int i);
//
//    bool insert(std::string i, t item);
//
    virtual bool insert(const std::vector<unsigned long long int>& Key, const std::vector<t>& item, std::vector<t>& fails );
//
//    virtual std::list<t> insert(std::list<unsigned long long int> Key, std::list<t> item);


};




template<class t>
HashTable<t>::~HashTable(){
    delete [] DATA;
    delete [] FLAG;
}

template<class t>
bool HashTable<t>::insertFinal(unsigned long long int Key, std::pair<unsigned long long int,t>& item){

//    std::pair<unsigned long long int, t> TEMP_ITEM(Key,item);

    /// Actual location Check.
    unsigned long long int place = Key;

    /// Make a Temporary Key for later check.
//    unsigned long long int keyCheck = Key;

    while(INSERTFLAG(place) && !DELETEFLAG(place)){
         place = DATA[place].first * ((place / DATA[place].first) + 1);
         if(place % TOTALSIZE == Key % TOTALSIZE)
             return false;
    }
    place = Key;
    while(true) {
        /// If there is no collision, set that location to Key and item.
        if(!INSERTFLAG(place) || DELETEFLAG(place)){
            DATA[place].second = item.second;
            DATA[place].first = item.first;
            SETINSERTFLAG(place);
            SETUNDELETEFLAG(place);
            SIZE += 1;
            return true;
        }
        /// IF there is a collison, swap the collision item with the temporary item and key.
        /// Search that items next possible location.
        else {
            /// Swap collison item with this item.


            item.second ^= DATA[place%TOTALSIZE].second;
            DATA[place%TOTALSIZE].second ^= item.second;
            item.second ^= DATA[place%TOTALSIZE].second;

            /// Swap collision Key with this item.
            DATA[place%TOTALSIZE].first ^= item.first;
            item.first ^= DATA[place%TOTALSIZE].first;
            DATA[place%TOTALSIZE].first ^= item.first;

            /// Set Place equal next place of the collision item.
            place = Key * ((place / Key ) + 1);
        }
    }
}

template<class t>
bool
HashTable<t>::insert( std::pair<unsigned long long int, t> item) {
//    std::cout << "Checking Rehash.\n";
    if(SIZE + 1 > TOTALSIZE / 2)
        rehash(SIZE);
//    std::cout << "Getting Key using " << i << "\n";
    unsigned long long int temp = getKey(item.first);
//    std::cout << "inserting Key = " << temp << " to Key vector\n";
    KEYORDER.emplace_back(temp);
//    std::cout << "Sending Key to Final insert\n";
    return (insertFinal( getKey(item.first), item ));
}

template<class t>
bool
HashTable<t>::insert( unsigned long long int Key, t item) {
//    std::cout << "Checking Rehash.\n";
    if(SIZE + 1 > TOTALSIZE / 2)
        rehash(SIZE);
//    std::cout << "Getting Key using " << i << "\n";
    unsigned long long int temp = getKey(Key);
//    std::cout << "inserting Key = " << temp << " to Key vector\n";
    KEYORDER.emplace_back(temp);
//    std::cout << "Sending Key to Final insert\n";
    return insertFinal( temp, std::pair<unsigned long long int, t >(Key,  item ));
}

template<class t>
bool
HashTable<t>::insertNoHash(std::pair<unsigned long long int, t> item) {
//    std::cout << "Getting Key using " << i << "\n";
    unsigned long long int temp = getKey(item.first);
//    std::cout << "inserting Key = " << temp << " to Key vector\n";
    KEYORDER.emplace_back(temp);
//    std::cout << "Sending Key to Final insert\n";
    return (insertFinal( temp, item ));
}

template<class t>
bool
HashTable<t>::insertNoHash(unsigned long long int Key, t item) {
//    std::cout << "Getting Key using " << i << "\n";
    unsigned long long int temp = getKey(Key);
//    std::cout << "inserting Key = " << temp << " to Key vector\n";
    KEYORDER.emplace_back(temp);
//    std::cout << "Sending Key to Final insert\n";
    return insertFinal( temp, std::pair<unsigned long long int, t>(Key, item));
}


template<class t>
unsigned long long int
HashTable<t>::getKey(unsigned long long int input) {
    return(( times_hashed * ((A * input * input * input * input) + (B * input * input * input) + (C * input* input) + (D * input) + input))%TOTALSIZE);
}

template<class t>
std::pair<t, bool> HashTable<t>::operator[](unsigned long long int Key) {
    if(DELETEFLAG(Key*times_hashed) || !INSERTFLAG(Key*times_hashed))
        return std::pair<t, bool>(t(),false);
    else
        return std::pair<t, bool>(DATA[Key*times_hashed].second,true);
}

template<class t>
bool HashTable<t>::insert(const std::vector<unsigned long long int>& Key,const std::vector<t>& item, std::vector<t>& fails) {
    if(item.size() != Key.size())
        throw std::runtime_error("Fail Vecter Insertion: parameter vectors of differing lengths");

    bool failed = true;

    if(Key.size() + SIZE >= TOTALSIZE / 2)
        rehash((static_cast<unsigned long long int>(Key.size()) + SIZE));

    auto j = Key.begin();
    for( auto i : item){
        if(!insertNoHash(std::pair<unsigned long long int, t>(*j, i))) {
            fails.emplace_back(i);
            failed = false;
        }
        ++j;
    }
    return(failed);
}

template<class t>
void
HashTable<t>::clear(unsigned long long int i) {
//    std::cout << "Soft deleting location " << i << "\n";
    SETDELETEFLAG(i);
}

template<class t>
void HashTable<t>::getAllofKey(unsigned long long int k, std::vector<t>& ret) {

    unsigned long long int Key = getKey(k);

    if(INSERTFLAG(Key)){
        if(!DELETEFLAG(Key)){
            ret.emplace_back(DATA[Key].second);}}

    for(unsigned long long int i = Key << 1; INSERTFLAG(Key << i) && (Key % TOTALSIZE != i % TOTALSIZE); i <<= 1){
        if(!DELETEFLAG(i) && (DATA[i%TOTALSIZE].first == Key))
            ret.emplace_back(DATA[i%TOTALSIZE].second);
    }

    return ret;
}


template<class t>
bool
HashTable<t>::isPrime( unsigned long long int temp) {
//    std::cout << "Test\n";
    if(temp == 2) {

        return true;
    }

//    std::cout << "Next Prime of: " << temp << "\n";
    if(!(temp & 0x1)) {
//        std::cout << "Return false: Divisible by 2\n";
        return false;

    }

//    std::cout << "Creating Threshold\n";
    unsigned long long int threshold = (temp / 3) +1;

//    std::cout << "Going through Loop.\n";
    for(unsigned long long int i = 3; i < threshold; i+=2){
//        std::cout << "Modulus\n";
        if(temp % i == 0) {
//            std::cout << "Return false: Divisible by: " << i << "\n";
            return false;
        }

    }
//    std::cout << "Return True\n";
    return true;
}


template<class t>
unsigned long long int
HashTable<t>::nextPrime(unsigned long long int b)
{
    unsigned long long int temp = b;
    while(++temp){
        if(isPrime(temp)) {
            return temp;
        }
    }
    return 0;
}

template<class t>
void
HashTable<t>::rehash(unsigned long long int Size){

    Size <<= 2;
    unsigned long long int temp_total_size = nextPrime(Size);
//    std::cout << "Temp Size is: " << Size << "\n";

//    std::cout << "Creating Data Array\n";
    std::vector< std::pair<unsigned long long int, t> > TEMP_DATA;


//    std::cout << "Setting Data Array to Temp Array.\n";
    for(unsigned long long int i = 0; i < TOTALSIZE; ++i) {
        if (INSERTFLAG(i) && !DELETEFLAG(i))
            TEMP_DATA.emplace_back(DATA[i]);
    }

//    std::cout << "Deleting the Data to original Hash.\n";
    delete [] DATA;
//    std::cout << "Deleting the Data to original Flag.\n";
    delete [] FLAG;

    DATA = new std::pair<unsigned long long int, t>[temp_total_size];

//    std::cout << "Creating Data Table of size: " << temp_total_size << "\n";
    FLAG = new unsigned char[Size];
//    std::cout << "Creating Flag Table of size: " << temp_total_size << "\n";

    TOTALSIZE = temp_total_size;


    times_hashed += 1;

//    std::cout << "Adding 1 to HashCount.\n";

//    std::cout << "Inserting Data Table old to New Table.\n";
    for(auto i = TEMP_DATA.begin(); i!= TEMP_DATA.end(); ++i)
        insertNoHash(*i);

//    std::cout << "Rehash Complete.\n";
}

#endif //HASHTABLE_HASHTABLE_H
